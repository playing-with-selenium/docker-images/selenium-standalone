ARG BROWSER_NAME
ARG SELENIUM_VERSION
FROM docker.io/selenium/standalone-${BROWSER_NAME}:${SELENIUM_VERSION}
ENV SCREEN_WIDTH=1920 SCREEN_HEIGHT=1080 SCREEN_DEPTH=32 SCREEN_DPI=300 START_XVFB=false
RUN sudo apt-get update && sudo apt-get -y install openjdk-11-jdk openjdk-11-jdk-headless
